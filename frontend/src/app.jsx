import React, { useEffect, useState } from 'react'

import './../styles/index.scss'
import Applications from './Applications'
import Map from './Map'
import { getList } from './services'

function App() {
    const [list, setList] = useState([])
    const [point, setPoint] = useState(null)
    const [size, setSize] = useState('simple')

    const handlePoint = (point) => {
        setPoint(point)
        console.log('handlePoint', point);
    }

    const fetchList = async () => {
        let listRes = await getList()
        setList(listRes)
    }

    const sizeChanged = (e) => {
        setSize(e.target.value);
        console.log(e.target.value);
    }

    useEffect(() => {
        fetchList()
    }, [])
    return (
        <div className="app">
            <div className="wrapper">
                <div className="Size">
                    <p className="Size__title">data size:</p>
                    <div className="Size__wrapper">
                        <div className="Size__item">
                            <label htmlFor="simple">2000</label>
                            <input type="radio" name="size" id="simple" checked={size === "simple"} value="simple" onChange={sizeChanged}/>
                        </div>
                        <div className="Size__item">
                            <label htmlFor="huge">5000</label>
                            <input type="radio" name="size" id="huge" checked={size === "huge"} value="huge" onChange={sizeChanged}/>
                        </div>
                    </div>
                </div>
                <Applications list={list} handlePoint={handlePoint} point={point} />
                <Map list={list} point={point} />
            </div>
        </div>
    );
}
export default App;