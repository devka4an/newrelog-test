import React, { useRef, useEffect, useState } from 'react';
import ReactMapGL, { Marker, Popup, ScaleControl, NavigationControl, Source, Layer } from 'react-map-gl'


import { clusterLayer, clusterCountLayer, unclusteredPointLayer } from './layers';
import { formatForClusterize } from './hooks/mapHooks'

const accessToken = 'pk.eyJ1Ijoia2E0YW5iZWsxMjMxMjMiLCJhIjoiY2wxeTA1OXJ3MDd6dTNqbGZkeDJmY2J3ZyJ9.SV9PwVoiB0PS6ewCHMdcUA';

const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
    c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
    C20.1,15.8,20.2,15.8,20.2,15.7z`;

const pinStyle = {
    cursor: 'pointer',
    fill: '#d00',
    stroke: 'none'
};

const Map = ({ point, list }) => {
    const [showPopup, setShowPopup] = React.useState(true);
    const mapRef = useRef(null);
    const [formattedMakers, setFormattedMakers] = useState([])

    const [viewport, setViewport] = useState({
        latitude: 43.26401375,
        longitude: 76.93508351,
        zoom: 12,
        bearing: 0,
        pitch: 0
    })

    const onClick = event => {
        const feature = event.features[0];
        const clusterId = feature.properties.cluster_id;

        const mapboxSource = mapRef.current.getSource('earthquakes');

        mapboxSource.getClusterExpansionZoom(clusterId, (err, zoom) => {
            if (err) {
                return;
            }

            mapRef.current.easeTo({
                center: feature.geometry.coordinates,
                zoom,
                duration: 500
            });
        });
    };

    useEffect(() => {
        setFormattedMakers(formatForClusterize(list))
    }, [list])

    useEffect(() => {
        if (point) {
            setViewport({
                latitude: point.coords.lat,
                longitude: point.coords.long,
                zoom: 13,
            })
        }
    }, [point])

    return (
        <div className="Map">
            <ReactMapGL
                {...viewport}
                mapStyle="mapbox://styles/mapbox/streets-v9"
                mapboxAccessToken={accessToken}
                interactiveLayerIds={[clusterLayer.id]}
                onClick={onClick}
                ref={mapRef}
                onMove={(evt) => setViewport(evt.viewState)}
            >
                {point &&
                    <Marker
                        longitude={point.coords.long}
                        latitude={point.coords.lat}
                        anchor="bottom"
                        isPinned={true}
                    >
                        <div className="marker">
                            <h3>{point.name}</h3>
                            <svg height={40} viewBox="0 0 24 24" style={pinStyle} onClick={() => { console.log('CLICKED'); }}>
                                <path d={ICON} />
                            </svg>
                        </div>

                        <Popup longitude={point.coords.long} latitude={point.coords.lat} offset={30}
                            anchor="bottom"
                            onClose={() => setShowPopup(false)}>
                            ID заказа: {point.id}
                        </Popup>
                    </Marker>
                }
                <Source
                    id="earthquakes"
                    type="geojson"
                    data={formattedMakers}
                    cluster={true}
                    clusterMaxZoom={14}
                    clusterRadius={50}
                >
                    <Layer {...clusterLayer} />
                    <Layer {...clusterCountLayer} />
                    <Layer {...unclusteredPointLayer} />
                </Source>
                <ScaleControl />
                <NavigationControl />
            </ReactMapGL>
        </div>
    );
};

export default Map;