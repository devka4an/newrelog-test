import React, { useEffect, useState } from 'react'
import {
    List,
    AutoSizer,
    CellMeasurer,
    CellMeasurerCache,
} from "react-virtualized";

export default function Applications({ list, handlePoint, point }) {

    const cache = React.useRef(
        new CellMeasurerCache({
            fixedWidth: true,
            defaultHeight: 100,
        })
    );
    const [apps, setApps] = React.useState([]);

    React.useEffect(() => {
        setApps([...list]);
    }, [list]);
    return (
        <div className="List">
            <div className="List__wrapper">
                <AutoSizer>
                    {({ width, height }) => (
                        <List
                            width={width}
                            height={height}
                            rowHeight={cache.current.rowHeight}
                            deferredMeasurementCache={cache.current}
                            rowCount={apps.length}
                            rowRenderer={({ key, index, style, parent }) => {
                                const app = apps[index];

                                return (
                                    <CellMeasurer
                                        key={key}
                                        cache={cache.current}
                                        parent={parent}
                                        columnIndex={0}
                                        rowIndex={index}
                                    >
                                        <div 
                                            className={`List__item Item ${point ? app.client_id == point.client_id ? 'active': '' : ''}`} 
                                            style={style}
                                            onClick={() => handlePoint(app)}>
                                            <p className="Item__title">{app.name} {app.client_id}</p>
                                            <p className="Item__type">type: <span>{app.type}</span></p>
                                            <p className="Item__price">price: <span>{app.price}</span></p>
                                        </div>
                                    </CellMeasurer>
                                );
                            }}
                        />
                    )}
                </AutoSizer>
            </div>
        </div>
    )
}
