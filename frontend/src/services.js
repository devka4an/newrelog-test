import axios from 'axios'

export const getList = async (size = 'simple') => {
    return await axios
        .get(`http://localhost:8081/list/${size}`)
        .then(res => {
            return res.data
        })

}