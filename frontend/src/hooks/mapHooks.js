import React, { createContext, useContext, useReducer } from "react";

const MapStateContext = createContext();

export const MapProvider = ({ children }) => {
    const [state, dispatch] = useReducer(MapReducer, { markers: [] });
    return (
        <MapStateContext.Provider value={state}>
        <MapDispatchContext.Provider value={dispatch}>
            {children}
        </MapDispatchContext.Provider>
        </MapStateContext.Provider>
    );
};

export const useStateMap = () => {
    const context = useContext(MapStateContext);

    if (context === undefined) {
        throw new Error("useStateMap must be used within a MapProvider");
    }
    return context;
};


export const formatForClusterize = (markers) => {
    let features = markers.map((marker, idx) => {
        return {
            type: 'Feature', 
            geometry: {
                type: 'Point', 
                coordinates: [ marker.coords.long, marker.coords.lat]
            }
        }
    })

    return {
        type: 'FeatureCollection',
        features 
        // [
        //     {type: 'Feature', geometry: {type: 'Point', coordinates: [-122.4, 37.8]}}
        // ]
    }
}
