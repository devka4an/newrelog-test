const path = require('path');
const {DefinePlugin} = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    mode: 'development',
    entry: './main.jsx',
    devtool: 'inline-source-map',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'bundle.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        static: './dist',
    },
    module: {
        rules: [
            {
                test: /\.js$|jsx/,
                exclude: /node_modules/,
                // loader: 'babel-loader',
                use: [{
                    loader: 'babel-loader',
                    options: {
                        cacheDirectory: true,
                        plugins: ['@babel/plugin-transform-runtime']
                    }
                }]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
            }
        ]
    },
    resolve: {
        extensions: ['.jsx', '.ts', '.js', '.css'],
    },
    plugins:[
        new HtmlWebpackPlugin({
            template: './index.html'
        }),
        new DefinePlugin({
            'process.env.MapboxAccessToken': 'pk.eyJ1Ijoia2E0YW5iZWsxMjMxMjMiLCJhIjoiY2wxeTA1OXJ3MDd6dTNqbGZkeDJmY2J3ZyJ9.SV9PwVoiB0PS6ewCHMdcUA'
        })
    ]
}