const express = require('express')
const app = express()
const cors = require('cors')

app.use(cors())

const apps2000 = require('./data/input-2000/NeRelog_apps.json')
const clients2000 = require('./data/input-2000/NeRelog_clients.json')
const apps5000 = require('./data/input-5000/NeRelog_apps.json')
const clients5000 = require('./data/input-5000/NeRelog_clients.json')

app.get('/list/:size', (req, res) => {
    let apps = []
    let clients = []

    if(req?.params?.size == 'huge') {
        apps = [...apps5000]
        clients = [...clients5000]
    } else {
        apps = [...apps2000]
        clients = [...clients2000]
    }


    let appsList = apps.map((item) => {
        let name = clients.find(x => x.id == item.client_id)?.name
        return {
            ...item,
            name
        }
    })

    res.send(appsList)
})

app.listen(8081, () => {
    console.log(`API RUNNNING ON http://localhost:8081`);
})